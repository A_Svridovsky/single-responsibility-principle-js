### Responsibilities of PersonController

Let's try to recognize current responsibilities of PersonController
and try to group them by reasons to change:

* RESTfull interface methods
  are exposed to external world and usually responsible of
  handling different request HTTP methods, URI mapping, request parameters parsing and
  error handling. Possible changes in requiremnts for RESTfull interfaces will lead to updates in PersonController.

* Data validation business logic
  has tendency to support more complex rules, error codes and descriptions as application grows.
  Such rules not always have direct correlation with business use cases and should be kept separately.

* Data operations and business logic
  might be less depended of data formats or validation rules changes
  but might significantly depended on business processes and use cases.

* Utility functions and formatting operations might be
  changed in case of email templates or configuration changes. 
